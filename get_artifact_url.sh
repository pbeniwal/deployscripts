artifactory_url="http://101.53.136.182:8081/artifactory"
artifactory_url="http://localhost:8081/artifactory"

#repo="libs-snapshot-local"
repo=${artifact_repo}

artifacts="${app_package}${app_name}"

url=$artifactory_url/$repo/$artifacts

file=`curl -s $url/maven-metadata.xml`

version=`curl -s $url/maven-metadata.xml | grep latest | sed "s/.*<latest>\([^<]*\)<\/latest>.*/\1/"`

build=`curl -s $url/$version/maven-metadata.xml | grep '<value>' |head -1 | sed "s/.*<value>\([^<]*\)<\/value>.*/\1/"`

BUILD_LATEST="$url/$version/${app_name}-$build.war"
echo "File Name  = " $BUILD_LATEST

echo $BUILD_LATEST > /tmp/filename.txt
